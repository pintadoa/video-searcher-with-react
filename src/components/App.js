import React from "react";
import SearchBar from "./SearchBar";
import VideoList from "./VideoList";
import VideoDetail from "./VideoDetail";
import youtube from "../apis/youtube";



class App extends React.Component {
  state = { videos: [], selectedVideo: null };

  componentDidMount(){
    this.onTermSubmit('buildings');
  }

  onTermSubmit = async (term) => {
      const respone = await youtube.get('/search', {
        params:{
          q: term
        }
      });

      this.setState({ 
        videos : respone.data.items,
        selectedVideo : respone.data.items[0]
      });
  };

  onVideoSelect = (video) =>{
    this.setState({ selectedVideo : video});
  };

  render() {
    return (
      <div className="ui container">
        <SearchBar onTermSubmit={this.onTermSubmit} />
        <div className="ui grid">
          <div className="ui row">
            <div className="ten wide column">
              <VideoDetail video={this.state.selectedVideo} />
            </div>
            <div className="six wide column">
              <VideoList onVideoSelect={this.onVideoSelect} videos={this.state.videos}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
