import axios from 'axios';

const KEY = 'AIzaSyCozCuLdU_umuq4SEJuV22lYerLocktD-Q';


export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 5,
        key: KEY
    }
});
